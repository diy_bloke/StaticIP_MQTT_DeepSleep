/*************************************************
 *    Template for static IP,MQTT &DeepSleep     *
 *      Read out a DHT11                         *  
 *************************************************/
#include <ESP8266WiFi.h>
#include <PubSubClient.h>
IPAddress ip(192,168,1,50);// pick your own IP outside the DHCP range of your router
IPAddress gateway(192,168,1,1);//watch out, these are comma's not dots
IPAddress subnet(255,255,255,0);
IPAddress dns(192,168,1,1);
//---------voor pubsub
WiFiClient EspClient;
PubSubClient client(EspClient);
//---------Other------
String naam = (__FILE__);     // filenaam
unsigned int batt;
ADC_MODE(ADC_VCC); // vcc uitlezen.
//--------DHT11------
float  humid_hok;
float temp_hok;
#include <DHT.h>
#define DHTTYPE  DHT11
#define DHTPIN 2 // D4
DHT dht(DHTPIN, DHTTYPE, 11); //


void setup() {
  Serial.begin(115200);
  //strip naam
  //strip path van filenaam
  byte p1 = naam.lastIndexOf('\\');
  byte p2 = naam.lastIndexOf('.');
  naam = naam.substring(p1 + 1, p2);
  batt = ESP.getVcc();
  // Set up WiFi
WiFi.config(ip,dns,gateway,subnet);
WiFi.begin("Your SSID","YourPW");
  //---------
client.setServer("192.168.1.103",1883);//your MQTT server's IP.Mind you, these are separated by dots again
//client.setCallback(callback);  
sendMQTTmessage();
/* Close WiFi connection */
  EspClient.stop();

ESP.deepSleep(15*1000000);//15 min
}

void loop() {

}

void sendMQTTmessage()
{
  if (!client.connected()) {
  reconnect();  }
  client.publish("home/hok/naam", naam.c_str(),false);
  client.publish("home/hok/stat/batt",String(batt/1000.0).c_str(),false);
  client.publish("home/hok/stat/temp",String(dht.readTemperature()).c_str(),false);
  client.publish("home/hok/stat/humid",String(dht.readHumidity()).c_str(),false);

   /* Close MQTT client cleanly */
      client.disconnect();
  }

  void reconnect(){
    while (!client.connected())
    {
      String ClientId="ESP8266";
      ClientId +=String(random(0xffff),HEX);
      if (client.connect(ClientId.c_str()))
      //if your MQTT server is protected with a password, use the next line instead of the revious
      //if (client.connect(ClientId.c_str()),mqtt_user,mqtt_password))
      {
        Serial.print("Connected");
        client.publish("home/hok/stat/connection","OK");
        }else{
          Serial.print("failed, rc= ");
          Serial.print(client.state());
          delay(1000);
          }
         
      }
    }

    
